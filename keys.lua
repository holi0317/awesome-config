local awful = require("awful")
local naughty = require("naughty")
local gears = require("gears")
local hotkeys_popup = require("awful.hotkeys_popup").widget
local globals = require("globals")
local shutdown_widget = require("widgets.shutdown")
local modkey = globals.modkey

local keys = {}

local awesome_keys = gears.table.join(
  awful.key({modkey}, "s", hotkeys_popup.show_help, {description = "show help", group = "awesome"}),
  awful.key({modkey, "Control", "Shift"}, "r", awesome.restart, {description = "reload awesome", group = "awesome"}),
  awful.key({modkey, "Control", "Shift"}, "q", function()
    local widget = shutdown_widget()
    widget:toggle()
  end, {description = "Show shutdown menu", group = "awesome"}),
  awful.key({}, "XF86PowerOff", function()
    local widget = shutdown_widget()
    widget:toggle()
  end, {description = "Show shutdown menu", group = "awesome"})
)

local tag_keys = gears.table.join()
-- Bind key numbers ot tags.
-- Be careful: we use keycodes to make it work on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
  local key = "#" .. i + 9

  local view_tag = awful.key({modkey}, key, function()
    local screen = awful.screen.focused()
    local tag = screen.tags[i]
    if tag then
      tag:view_only()
    end
  end, {description = "View tag #" .. i, group = "tag"})

  local move_client = awful.key({modkey, "Shift"}, key, function()
    if client.focus then
      local tag = client.focus.screen.tags[i]
      if tag then
        client.focus:move_to_tag(tag)
      end
    end
  end, {description = "move focused client to tag #" .. i, group = "tag"})

  local toggle_focus = awful.key({modkey, "Control", "Shift"}, key, function()
    if client.focus then
      local tag = client.focus.screen.tags[i]
      if tag then
        client.focus:toggle_tag(tag)
      end
    end
  end, {description = "toggle focused client on tag #" .. i, group = "tag"})

  tag_keys = gears.table.join(
    tag_keys,
    view_tag,
    move_client,
    toggle_focus
  )
end


local client_keys = gears.table.join(
  awful.key({modkey}, "j", function()
    awful.client.focus.byidx(1)
  end, {description = "focus next by index", group = "client"}),

  awful.key({modkey}, "k", function()
    awful.client.focus.byidx(-1)
  end, {description = "focus previous by index", group = "client"}),

  -- Layout manipulation
  awful.key({modkey, "Shift"}, "j", function()
    awful.client.swap.byidx(1)
  end, {description = "swap with next client by index", group = "client"}),

  awful.key({modkey, "Shift"}, "k", function()
    awful.client.swap.byidx(-1)
  end, {description = "swap with previous client by index", group = "client"}),

  --awful.key({modkey}, "u", awful.client.urgent.jumpto, {description = "jump to urgent client", group = "client"}),

  awful.key({modkey, "Control"}, "b", function()
    local c = awful.client.restore()
    -- Focus restored client
    if c then
      client.focus = c
      c:raise()
    end
  end, {description = "restore minimized", group = "client"}),

  awful.key({modkey}, "i", function()
    local c = client.focus
    if c == nil then
      return
    end

    local values = {
      Floating = c.floating,
      Maximized = c.maximized,
      Fullscreen = c.fullscreen,
      Sticky = c.sticky,
      Modal = c.modal,
      Ontop = c.ontop,
      Below = c.below,
      Hidden = c.hidden,
      Minimized = c.minimized,
      Urgent = c.urgent,
      Focusable = c.focusable
    }

    local content = ""
    for name, value in pairs(values) do
      content = content .. string.format("%s: %s\n", name, value)
    end

    naughty.notify({
      text = content,
      title = client.name
    })
  end, {description = "Show info of focused client", group = "client"})
)

local screen_keys = gears.table.join(
  awful.key({modkey, "Control"}, "j", function()
    awful.screen.focus_relative(1)
  end, {description = "focus the next screen", group = "screen"}),

  awful.key({modkey, "Control"}, "k", function()
    awful.screen.focus_relative(-1)
  end, {description = "focus the previous screen", group = "screen"}),

  awful.key({modkey, "Shift", "Control"}, "j", function()
    local c = client.focus
    if c == nil then
      return
    end
    c:move_to_screen()
  end, {description = "move client to next screen", group = "screen"}),

  awful.key({modkey, "Shift", "Control"}, "k", function()
    local c = client.focus
    if c == nil then
      return
    end
    local target = c.screen.index - 1
    c:move_to_screen(target)
  end, {description = "move client to previous screen", group = "screen"})
)

local launcher_keys = gears.table.join(
  awful.key({modkey}, "Return", function()
    awful.spawn(globals.terminal)
  end, {description = "open a terminal", group = "launcher"}),

  awful.key({modkey}, "x", function()
    awful.spawn("xset s activate")
  end, {description = "lock screen", group = "launcher"}),

  awful.key({}, "Print", function()
    awful.spawn.with_shell(globals.screenshot)
  end, {description = "Screenshot", group = "launcher"}),

  awful.key({modkey}, "d", function()
    awful.spawn("rofi -show drun")
  end, {description = "Show rofi application menu", group = "launcher"}),

  awful.key({modkey}, "p", function()
    awful.spawn("rofi-pass")
  end, {description = "Show pass entries", group = "launcher"}),

  awful.key({}, "XF86Calculator", function()
    awful.spawn("speedcrunch")
  end, {description = "Launch calculator", group = "launcher"}),

  awful.key({modkey}, ",", function()
    awful.spawn("rofimoji")
  end, {description = "Launch emoji selector", group = "launcher"})
)

local layout_keys = gears.table.join(
  awful.key({modkey}, "l", function()
    awful.tag.incmwfact(0.05)
  end, {description = "increase master width factor", group = "layout"}),

  awful.key({modkey}, "h", function()
    awful.tag.incmwfact(-0.05)
  end, {description = "decrease master width factor", group = "layout"}),

  awful.key({modkey, "Shift"}, "h", function()
    awful.tag.incnmaster(1, nil, true)
  end, {description = "increase the number of master clients", group = "layout"}),

  awful.key({modkey, "Shift"}, "l", function()
    awful.tag.incnmaster(-1, nil, true)
  end, {description = "decrease the number of master clients", group = "layout"}),

  awful.key({modkey, "Control"}, "h", function()
    awful.tag.incncol(1, nil, true)
  end, {description = "increase the number of columns", group = "layout"}),

  awful.key({modkey, "Control"}, "l", function()
    awful.tag.incncol(-1, nil, true)
  end, {description = "decrease the number of columns", group = "layout"}),

  awful.key({modkey}, "space", function()
    awful.layout.inc(1)
  end, {description = "select next", group = "layout"}),

  awful.key({modkey, "Shift"}, "space", function()
    awful.layout.inc(-1)
  end, {description = "select previous", group = "layout"})
)

local audio_keys = gears.table.join(
  -- Lower volume
  awful.key({}, "XF86AudioLowerVolume", function()
    os.execute("pactl set-sink-volume @DEFAULT_SINK@ -5%")
    awful.screen.focused().pulse_update()
  end, {description = "Lower volume", group = "volume"}),

  -- Raise volume
  awful.key({}, "XF86AudioRaiseVolume", function()
    os.execute("pactl set-sink-volume @DEFAULT_SINK@ +5%")
    awful.screen.focused().pulse_update()
  end, {description = "Raise volume", group = "volume"}),

  -- Mute
  awful.key({}, "XF86AudioMute", function()
    os.execute("pactl set-sink-mute @DEFAULT_SINK@ toggle")
    awful.screen.focused().pulse_update()
  end, {description = "Mute", group = "volume"}),

  -- Last track
  awful.key({}, "XF86AudioNext", function()
    os.execute("mocp -f")
    awful.screen.focused().moc_update()
  end, {description = "Last track", group = "volume"}),

  -- Play/pause track
  awful.key({}, "XF86AudioPlay", function()
    os.execute("mocp -G")
    awful.screen.focused().moc_update()
  end, {description = "Toggle play/pause", group = "volume"}),

  -- Next track
  awful.key({}, "XF86AudioPrev", function()
    os.execute("mocp -r")
    awful.screen.focused().moc_update()
  end, {description = "Next track", group = "volume"})
)

local bright_keys = gears.table.join(
  -- Dim monitor
  awful.key({}, "XF86MonBrightnessDown", function()
    awful.spawn("xbacklight -dec 10")
  end, {description = "Dim monitor", group = "Brightness"}),

  -- Bright moniter
  awful.key({}, "XF86MonBrightnessUp", function()
    awful.spawn("xbacklight -inc 10")
  end, {description = "Bright monitor", group = "Brightness"})
)

local naughty_keys = gears.table.join(
  awful.key({modkey}, "n", function()
    naughty.destroy_all_notifications(nil, naughty.notificationClosedReason.dismissedByUser)
  end, {description = "Dismiss all notification", group = "naughty"})
)

keys.globalkeys = gears.table.join(
  awesome_keys,
  tag_keys,
  client_keys,
  screen_keys,
  launcher_keys,
  layout_keys,
  audio_keys,
  bright_keys,
  naughty_keys
)

keys.clientkeys = gears.table.join(
  awful.key({modkey}, "f", function(c)
    c.fullscreen = not c.fullscreen
    c:raise()
  end, {description = "toggle fullscreen", group = "client"}),

  awful.key({modkey, "Shift"}, "q", function(c)
    c:kill()
  end, {description = "close", group = "client"}),

  awful.key({modkey, "Control"}, "space",
    awful.client.floating.toggle,
  {description = "toggle floating", group = "client"}),

  awful.key({modkey, "Control"}, "Return", function(c)
    c:swap(awful.client.getmaster())
  end, {description = "move to master", group = "client"}),

  awful.key({modkey}, "o", function(c)
    c:move_to_screen()
  end, {description = "move to screen", group = "client"}),

  awful.key({modkey}, "t", function(c)
    c.ontop = not c.ontop
  end, {description = "toggle keep on top", group = "client"}),

  awful.key({modkey}, "b", function(c)
    -- The client currently has the input focus, so it cannot be
    -- minimized, since minimized clients can't have the focus.
    c.minimized = true
  end, {description = "minimize", group = "client"}),

  awful.key({modkey}, "m", function(c)
    c.maximized = not c.maximized
    c:raise()
  end, {description = "(un)maximize", group = "client"}),

  awful.key({modkey, "Control"}, "m", function(c)
    c.maximized_vertical = not c.maximized_vertical
    c:raise()
  end, {description = "(un)maximize vertically", group = "client"}),

  awful.key({modkey, "Shift"}, "m", function(c)
    c.maximized_horizontal = not c.maximized_horizontal
    c:raise()
  end, {description = "(un)maximize horizontally", group = "client"}),

  awful.key({modkey}, "Up", function(c)
    if c.floating then
      c.y = math.max(c.y - 10, -c.height)
    end
  end, {description = "Move up float window", group = "client"}),

  awful.key({modkey}, "Down", function(c)
    if c.floating then
      c.y = math.min(c.y + 10, c.screen.geometry.height)
    end
  end, {description = "Move down float window", group = "client"}),

  awful.key({modkey}, "Left", function(c)
    if c.floating then
      c.x = math.max(c.x - 10, -c.width)
    end
  end, {description = "Move left float window", group = "client"}),

  awful.key({modkey}, "Right", function(c)
    if c.floating then
      c.x = math.min(c.x + 10, c.screen.geometry.width)
    end
  end, {description = "Move right float window", group = "client"}),

  awful.key({modkey, "Control"}, "Up", function(c)
    c.height = c.height - 10
  end, {description = "Decrease height of window", group = "client"}),

  awful.key({modkey, "Control"}, "Down", function(c)
    c.height = c.height + 10
  end, {description = "Increase height of window", group = "client"}),

  awful.key({modkey, "Control"}, "Right", function(c)
    c.width = c.width + 10
  end, {description = "Increase width of window", group = "client"}),

  awful.key({modkey, "Control"}, "Left", function(c)
    c.width = c.width - 10
  end, {description = "Decrease width of window", group = "client"})
)

keys.clientbuttons = gears.table.join(
  awful.button({}, 1, function(c)
    client.focus = c
    c:raise()
  end),

  awful.button({modkey}, 1, awful.mouse.client.move),

  awful.button({modkey, "Control"}, 1, awful.mouse.client.resize)
)

root.keys(keys.globalkeys)

return keys
