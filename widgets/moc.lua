local wibox = require("wibox")
local awful = require("awful")
local gears = require("gears")
local lain = require("lain")
local globals = require("globals")
local colors = require("colors")

local markup = lain.util.markup

-- Factory function for moc (Music on console) widget
-- @param color Text color for moc status
-- @param tt_fg Frontground color for tooltip
-- @param tt_bg Background color for tooltip
local function moc_widget(color, tt_fg, tt_bg)
  color = color or colors.white
  tt_fg = tt_fg or colors.white
  tt_bg = tt_bg or colors.black

  local tooltip = awful.tooltip({})
  tooltip.fg = tt_fg
  tooltip.bg = tt_bg
  local moc = lain.widget.contrib.moc({
    timeout = 10,
    music_dir = "~/Music",
    followtag = true,
    settings = function()
      -- luacheck: globals moc_now widget moc_notification_preset
      local state = moc_now.state
      local title = moc_now.title
      local album = moc_now.album
      local artist = moc_now.artist

      if title == "" then
        -- Only show filename as title
        title = moc_now.file:match("^.+/(.+)$")
      end
      if album == "" then
        album = "<Unknown>"
      end
      if artist == "" then
        artist = "<Unknown>"
      end


      if state == "STOP" then
        widget:set_markup(markup.fg.color(color, "栗"))
        tooltip.text = "MOC: Stopped"
        return
      elseif state == "PAUSE" then
        widget:set_markup(markup.fg.color(color, ""))
        tooltip.text = string.format("MOC: Paused\nTrack: %s\nArtist: %s\nAlbum: %s", title, artist, album)
        return
      end

      widget:set_markup(markup.fg.color(color, "契 " .. title))
      tooltip.text = string.format("MOC: Playing\nTrack: %s\nArtist: %s\nAlbum: %s", title, artist, album)

      moc_notification_preset = {
        title = "Now playing",
        timeout = 6,
        text = string.format("%s (%s) - %s\n%s", artist, album, moc_now.total, title)
      }
    end
  })

  local wrapped_moc = wibox.container.constraint(moc.widget, "max", 200)
  tooltip:add_to_object(wrapped_moc)

  wrapped_moc:buttons(gears.table.join(
    awful.button({}, 1, function()
      awful.spawn(globals.terminal .. " -e mocp")
    end)
  ))

  return wrapped_moc, moc.update
end

return moc_widget
