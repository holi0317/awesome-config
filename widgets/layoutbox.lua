local awful = require("awful")
local gears = require("gears")

-- Factory function for creating a layoutbox widget from awful
-- @param s Screen object provided by awesome
local function layoutbox_widget(s)
  local layoutbox = awful.widget.layoutbox(s)
  layoutbox:buttons(gears.table.join(
    awful.button({}, 1, function()
      awful.layout.inc(1)
    end),

    awful.button({}, 2, function()
      awful.layout.set(awful.layout.layouts[1])
    end),

    awful.button({}, 3, function()
      awful.layout.inc(-1)
    end),

    awful.button({}, 4, function()
      awful.layout.inc(1)
    end),

    awful.button({}, 5, function()
      awful.layout.inc(-1)
    end)
  ))

  return layoutbox
end

return layoutbox_widget
