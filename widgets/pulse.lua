local gears = require("gears")
local awful = require("awful")
local lain = require("lain")
local colors = require("colors")
local globals = require("globals")

local markup = lain.util.markup

-- Average volumes from given channels
-- @param channels volume_now.channel table from lain pulse widget
local function avg_volume(channels)
  local volume = 0
  local count = 0

  for _, device in ipairs(channels) do
    local vol = tonumber(device)
    if vol ~= nil then
      volume = volume + vol
      count = count + 1
    end
  end

  if count > 0 then
    return math.floor(volume / count)
  else
    return 0
  end
end

-- Factory function for pulse audio volume display widget
-- When building this widget, an pulsebar widget from lain is also built for notification
-- @param color Color when volume is not muted
-- @param muted_color Color when speaker is muted
-- @param notify_fg Frontground color for volume changed notification
-- @param notify_bg Background color for volume changed notification
local function pulse_widget(color, muted_color, notify_fg, notify_bg)
  color = color or colors.white
  muted_color = muted_color or colors.yellow_500
  notify_fg = notify_fg or colors.white
  notify_bg = notify_bg or colors.black

  local pulse = lain.widget.pulse({
    settings = function()
      -- luacheck: globals volume_now widget
      local volume = avg_volume(volume_now.channel)

      -- Luacheck false report unused variable text
      -- luacheck: no unused
      local text = ""
      local c = color

      if volume_now.muted == "no" then
        local icon = "奄"

        if volume > 50 then
          icon = "墳"
        elseif volume > 30 then
          icon = "奔"
        end

        text = icon .. " " .. volume .. "%"
      else
        text = "婢 Mute"
        c = muted_color
      end

      widget:set_markup(markup.fg.color(c, text))
    end
  })

  local pulsebar = lain.widget.pulsebar({
    notification_preset = {
      font = globals.fontname .. "12",
      fg = notify_fg,
      bg = notify_bg
    },
  })

  local function update()
    pulse.update()
    pulsebar.notify()
  end

  pulse.widget:buttons(gears.table.join(
    -- Left click
    awful.button({}, 1, function()
      awful.spawn("pavucontrol-qt")
    end),

    -- Right click
    awful.button({}, 3, function()
      os.execute(string.format("pactl set-sink-mute %d toggle", pulse.device))
      update()
    end),

    -- Scroll up
    awful.button({}, 4, function()
      os.execute(string.format("pactl set-sink-volume %d +1%%", pulse.device))
      update()
    end),

    -- Scroll down
    awful.button({}, 5, function()
      os.execute(string.format("pactl set-sink-volume %d -1%%", pulse.device))
      update()
    end)
  ))

  return pulse.widget, update
end

return pulse_widget
