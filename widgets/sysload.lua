local awful = require("awful")
local lain = require("lain")
local colors = require("colors")

local markup = lain.util.markup

-- Factory function for sysload widget
-- @param color Frontground color when sysload is normal
-- @param mid_color Frontground color when sysload is high
-- @parm high_color Frontground color when sysload is really high
local function sysload_widget(color, mid_color, high_color)
  color = color or colors.white
  mid_color = mid_color or colors.white
  high_color = high_color or colors.white

  local function load_to_color(loading)
    local l = tonumber(loading) or 0

    if l > 10 then
      return high_color
    elseif l > 4 then
      return mid_color
    else
      return color
    end
  end

  local tooltip = awful.tooltip({})
  local sysload = lain.widget.sysload({
    settings = function()
      -- luacheck: globals load_1 load_5 load_15 widget
      local c = load_to_color(load_1)
      widget:set_markup(markup.fg.color(c, "龍  " .. load_1 .. " "))

      -- FIXME Color is not working here
      --tooltip.text = string.format(
        --"1 minute: %s\n5 minutes: %s\n15 minutes: %s",
        --markup.fg.color(load_to_color(load_1), load_1),
        --markup.fg.color(load_to_color(load_5), load_5),
        --markup.fg.color(load_to_color(load_15), load_15)
      --)
      tooltip.text = string.format(
        "1 minute: %s\n5 minutes: %s\n15 minutes: %s",
        load_1,
        load_5,
        load_15
      )
    end
  })
  tooltip:add_to_object(sysload.widget)

  return sysload.widget
end

return sysload_widget
