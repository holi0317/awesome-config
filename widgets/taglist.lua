local awful = require("awful")

-- Factory function for taglist widget
-- @param s Screen object for the tag list
local function taglist_widget(s)
  return awful.widget.taglist({
    screen = s,
    filter = awful.widget.taglist.filter.all,
    buttons = awful.util.taglist_buttons
  })
end

return taglist_widget
