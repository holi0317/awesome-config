local awful = require("awful")
local lain = require("lain")
local colors = require("colors")
local fs = require("gears.filesystem")

local markup = lain.util.markup

-- Get icon reprecenting current battery percent
-- @param perc Battery percent in integer
local function icon_from_juice(perc)
  if perc == "N/A" then
    return nil
  end

  if perc > 90 then
    return ""
  elseif perc > 80 then
    return ""
  elseif perc > 70 then
    return ""
  elseif perc > 60 then
    return ""
  elseif perc > 50 then
    return ""
  elseif perc > 40 then
    return ""
  elseif perc > 30 then
    return ""
  elseif perc > 20 then
    return ""
  elseif perc > 10 then
    return ""
  else
    return ""
  end
end

-- Get color from current battery percent
-- @param perc Batter percent in integer
local function color_from_juice(perc)
  if perc == "N/A" then
    return nil
  end

  if perc > 30 then
    return colors.white
  elseif perc > 15 then
    return colors.yellow_500
  else
    return colors.red_500
  end
end

local charging_icon = ""

-- Factory function for battery widget
-- If battery is not found (Directory `/sys/class/power_supply/BAT0` is not found), nil will be returned
local function battery_widget()
  if not fs.is_dir("/sys/class/power_supply/BAT0") then
    return nil
  end

  local tooltip = awful.tooltip({})
  local bat = lain.widget.bat({
    settings = function()
      -- luacheck: globals bat_now widget
      local color = color_from_juice(bat_now.perc) or colors.blue_grey_700
      local icon = icon_from_juice(bat_now.perc) or ""

      if bat_now.ac_status == 1 then
        icon = charging_icon
        color = colors.green_500
      end

      widget:set_markup(markup.fg.color(color, icon .. " " .. bat_now.perc .. "% "))

      tooltip.text = string.format("Status: %s\nTime remaining: %s", bat_now.status, bat_now.time)
    end
  })
  tooltip:add_to_object(bat.widget)

  return bat.widget
end

return battery_widget
