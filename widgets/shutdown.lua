local awful = require("awful")

local function to_spawn(cmd)
  return function()
    awful.spawn(cmd)
  end
end

local function quit()
  awesome.quit()
end

local entries = {
  { " Logout", quit },
  { " Lock", to_spawn("xset s activate") },
  { "鈴 Suspend", to_spawn("systemctl suspend") },
  { " Hibernate", to_spawn("systemctl hibernate") },
  { "襤 Shutdown", to_spawn("systemctl poweroff") },
  { " Reboot", to_spawn("systemctl reboot") }
}

local menu = nil

-- Get shutdown widget instance.
-- The widget should be a singleton. This will cache the result of
-- widget and ensure there is only one shutdown widget in action.
-- @returns awful.menu instance
return function()
  if menu == nil then
    menu = awful.menu(entries)
  end
  return menu
end
