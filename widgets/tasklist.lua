local awful = require("awful")
local wibox = require("wibox")
local utils = require("utils")

-- Signals to be watched for updating tag list tooltip
local SIGNALS = {
  "focus",
  "list",
  "swapped",
  "manage",
  "tagged",
  "unfocus",
  "untagged",
  "lowered",
  "property::name"
}

-- Icon mappings. Order determine which icon goes first
local ICONS = {
  sticky = "",
  ontop = "",
  above = "",
  below = "",
  floating = "",
  maximized = "",
  maximized_horizontal = "",
  maximized_vertical =  "",
  minimized = "",
}

-- Generate client description for tooltip
-- The generated description should fit in a line
-- (i.e. No \n would be appended before or after the produced string)
--
-- @param c client object provided by awesome
-- @returns string for client description. May contain nerdfont glyph
local function get_client_desc(c)
  local name = c.name
  if name == nil then
    name = ""
  end
  local icons = ""

  for key, ico in pairs(ICONS) do
    if c[key] then
      icons = icons .. ico
    end
  end

  if icons == "" then
    icons = "•"
  end

  return " " .. icons .. " " .. name
end

-- Factory function for tasklist widget
-- @param s Awful screen object
local function tasklist_widget(s)
  local tasklist = awful.widget.tasklist(s, awful.widget.tasklist.filter.focused, awful.util.tasklist_buttons)

  local tooltip = awful.tooltip({})
  local wrapped_tasklist = wibox.container.constraint(tasklist, "max", 300)
  tooltip:add_to_object(wrapped_tasklist)

  -- Function to update tasklist widget tooltip for current screen
  local function update()
    local result = ""
    for _, c in ipairs(s.clients) do
      -- FIXME: Also include minimized clients
      if awful.widget.tasklist.filter.currenttags(c, s) and c.type ~= "splash" and c.type ~= "dock" and c.type ~= "desktop" then
        if result ~= "" then
          result = result .. "\n"
        end
        result = result .. get_client_desc(c)
      end
    end

    tooltip.text = result
  end
  local debounced_update = utils.debounce(update, 0.2)

  for _, signal in pairs(SIGNALS) do
    client.connect_signal(signal, debounced_update)
  end

  return wrapped_tasklist
end

return tasklist_widget
