local wibox = require("wibox")
local lain = require("lain")
local colors = require("colors")
local globals = require("globals")

local markup = lain.util.markup

-- Clock widget factory
-- @param color Color of frontground text
-- @param cal_fg Color for frontground color on calendar
-- @param cal_bg Color for background color on calendar
local function clock_widget(color, cal_fg, cal_bg)
  color = color or colors.white
  cal_fg = cal_fg or colors.white
  cal_bg = cal_bg or colors.black

  local clock = wibox.widget.textclock(markup.fg.color(color, "  %a %b %d, %H:%M"))

  -- Attach calendar widget to clock
  lain.widget.cal({
    --cal = "cal --color=always",
    attach_to = { clock },
    week_start = 1,
    followtag = true,
    notification_preset = {
      font = globals.fontname .. "12",
      fg   = cal_fg,
      bg   = cal_bg
    }
  })

  return clock
end

return clock_widget
