local gears = require("gears")
local awful = require("awful")
local wibox = require("wibox")
local naughty = require("naughty")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi
local globals = require("globals")
local colors = require("colors")

local gfs = require("gears.filesystem")
local themes_path = gfs.get_themes_dir()

local taglist_widget = require("widgets.taglist")
local layoutbox_widget = require("widgets.layoutbox")
local clock_widget = require("widgets.clock")
local pulse_widget = require("widgets.pulse")
local battery_widget = require("widgets.battery")
local sysload_widget = require("widgets.sysload")
local tasklist_widget = require("widgets.tasklist")
local moc_widget = require("widgets.moc")

local icon_path = os.getenv("HOME") .. "/.config/awesome/icons/"

local theme = {}

theme.font          = globals.font

theme.bg_normal     = colors.blue_grey_700
theme.bg_focus      = colors.pink_600
theme.bg_urgent     = theme.light_blue_600
theme.bg_minimize   = theme.bg_focus
theme.bg_systray    = colors.pink_400

theme.fg_normal     = "#212121"
theme.fg_focus      = "#f5f5f5"
theme.fg_urgent     = "#f5f5f5"
theme.fg_minimize   = "#f5f5f5"

theme.useless_gap   = dpi(15)
theme.border_width  = dpi(1)
theme.border_normal = colors.black
theme.border_focus  = colors.purple_400
theme.border_marked = colors.red_400
theme.border_radius = dpi(6)
theme.border_shape = function(cr, width, height)
  gears.shape.rounded_rect(cr, width, height, theme.border_radius)
end

theme.hotkeys_bg = colors.deep_purple_200
theme.hotkeys_font = globals.fontname .. "12"
theme.hotkeys_description_font = "Noto sans 12"

theme.menu_height = dpi(20)
theme.menu_width  = dpi(150)
theme.menu_fg_normal = colors.white
theme.menu_bg_normal = colors.blue_grey_700
theme.menu_fg_focus = colors.white
theme.menu_bg_focus = colors.pink_500

theme.notification_bg = colors.orange_500
theme.notification_fg = colors.white
theme.notification_opacity = 0.9
theme.notification_max_width = 600
theme.notification_max_height = 400
theme.notification_icon_size = 64
theme.notification_margin = dpi(5)
theme.notification_shape = theme.border_shape
naughty.config.presets.low.bg = colors.cyan_500
naughty.config.presets.critical.bg = colors.red_500

-- Define the image to load
theme.awesome_icon = icon_path .. "awesome.png"
theme.menu_submenu_icon = icon_path .. "submenu.png"

theme.layout_fairh = themes_path.."default/layouts/fairhw.png"
theme.layout_fairv = themes_path.."default/layouts/fairvw.png"
theme.layout_floating  = themes_path.."default/layouts/floatingw.png"
theme.layout_magnifier = themes_path.."default/layouts/magnifierw.png"
theme.layout_max = themes_path.."default/layouts/maxw.png"
theme.layout_fullscreen = themes_path.."default/layouts/fullscreenw.png"
theme.layout_tilebottom = themes_path.."default/layouts/tilebottomw.png"
theme.layout_tileleft   = themes_path.."default/layouts/tileleftw.png"
theme.layout_tile = themes_path.."default/layouts/tilew.png"
theme.layout_tiletop = themes_path.."default/layouts/tiletopw.png"
theme.layout_spiral  = themes_path.."default/layouts/spiralw.png"
theme.layout_dwindle = themes_path.."default/layouts/dwindlew.png"
theme.layout_cornernw = themes_path.."default/layouts/cornernww.png"
theme.layout_cornerne = themes_path.."default/layouts/cornernew.png"
theme.layout_cornersw = themes_path.."default/layouts/cornersww.png"
theme.layout_cornerse = themes_path.."default/layouts/cornersew.png"

theme.wallpaper = theme.bg_normal

theme.tasklist_bg_normal = colors.purple_400
theme.tasklist_bg_focus = theme.tasklist_bg_normal
theme.tasklist_bg_urgent = theme.tasklist_bg_urgent
theme.tasklist_plain_task_name = true
theme.tasklist_disable_icon = true

theme.taglist_fg_empty = theme.fg_focus
theme.taglist_fg_volatile = theme.taglist_fg_empty
theme.taglist_fg_occupied = theme.taglist_fg_empty

-- Define the icon theme for application icons. If not set then the icons
-- from /usr/share/icons and /usr/share/icons/hicolor will be used.
theme.icon_theme = "Papirus"

-- Draw a powerline container
local function pl(widget, bgcolor, reversed)
  local function shape(cr, width, height)
    local depth = height / 2
    if reversed then
      depth = -1 * depth
    end
    gears.shape.powerline(cr, width, height, depth)
  end

  return wibox.container.background(wibox.container.margin(widget, 16, 16), bgcolor, shape)
end

local function pl_head(widget, bgcolor, reversed)
  local function shape(cr, width, height)
    local arrow_length = height/2
    if reversed then
      cr:move_to(0, height / 2)
      cr:line_to(arrow_length, 0)
      cr:line_to(width, 0)
      cr:line_to(width, height)
      cr:line_to(arrow_length , height)
    else
      cr:move_to(0, 0)
      cr:line_to(width - arrow_length, 0)
      cr:line_to(width, height / 2)
      cr:line_to(width - arrow_length, height)
      cr:line_to(0, height)
    end

    cr:close_path()
  end

  return wibox.container.background(wibox.container.margin(widget, 16, 16), bgcolor, shape)
end

-- Create array of powerline elements that should be displayed on the right
-- Accept array of wibox widgets.
-- If an element is nil, it will be filtered away.
-- If an element is a function, it will be called with its color in the powerline area
local function pls(elements)
  local filtered_elements = {}
  local result = {}
  local bar_colors = {
    colors.pink_900,
    colors.pink_800,
    colors.pink_700,
    colors.pink_600,
    colors.pink_500,
    colors.pink_400,
    colors.pink_300
  }
  local fallback_color = bar_colors[#bar_colors]

  for _, el in pairs(elements) do
    if el ~= nil then
      filtered_elements[#filtered_elements + 1] = el
    end
  end

  for i, el in pairs(filtered_elements) do
    local color = bar_colors[#filtered_elements + 1 - i]
    if color == nil then
      color = fallback_color
    end

    local resolved = el
    if type(el) == "function" then
      resolved = el(color)
    end

    local pl_fn = pl
    if i == #filtered_elements then
      pl_fn = pl_head
    end
    result[#result + 1] = pl_fn(resolved, color, true)
  end

  result.layout = wibox.layout.fixed.horizontal
  return result
end

function theme.at_screen_connect(s)
  -- Because systray widget does not support passing in color as argument.
  -- This is a factory fucntion decided for function `pls` which correctly set systray color.
  local function systray_factory(color)
    theme.bg_systray = color
    return wibox.widget.systray()
  end

  if screen.primary ~= s then
    systray_factory = nil
  end

  -- Create the wibox
  local bar = awful.wibar({
    position = "top",
    screen = s,
    height = s.dpi * 0.2,
    bg = theme.bg_normal,
    fg = theme.fg_normal
  })

  local pulse, pulse_update = pulse_widget(colors.white, colors.amber_A700, colors.white, colors.orange_500)
  s.pulse_update = pulse_update

  local moc, moc_update = moc_widget(colors.white, colors.white, colors.teal_700)
  s.moc_update = moc_update

  -- Add widgets to the wibox
  bar:setup {
    layout = wibox.layout.align.horizontal,
    { -- Left widgets
      layout = wibox.layout.fixed.horizontal,
      pl_head(taglist_widget(s), colors.purple_500),
      pl(tasklist_widget(s), theme.tasklist_bg_normal)
    },
    nil,
    pls({ -- Right widgets
      moc,
      systray_factory,
      sysload_widget(colors.white, colors.amber_A700, colors.yellow_A700),
      battery_widget(),
      pulse,
      clock_widget(colors.white, colors.white, colors.purple_700),
      layoutbox_widget(s)
    })
  }
end

return theme
