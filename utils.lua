local timer = require("gears.timer")
local protected_call = require("gears.protected_call")
local utils = {}

-- Create a debounced function, similar to lodash debounce from JS.
-- The function will be queued for calling at timeout. Subsequent call
-- to the returned function will does nothing to the queue until it is called.
--
-- @param func Function to be called. The function signature should be `() => void`
-- @param timeout Time (in seconds) to debounce before actually calling the function
-- @returns Debounced function with signature `() => void` which is debounced version of
-- provided function
function utils.debounce(func, timeout)
  local time = nil

  return function()
    -- No-op when calling it on scheduled
    if time ~= nil then
      return
    end

    time = timer.start_new(timeout, function()
      protected_call.call(func)
      time = nil
      return false
    end)
  end
end

local hostname = nil
-- Get hostname from /etc/hostname and execute callback function
--
-- If the hostname is not defined or error when reading hostname,
-- empty string will be returned
--
-- Otherwise, hostname will be returned with potential tailing new line
-- removed.
--
-- This function will cache the result of hostname if reading succeed.
--
-- @returns hostname in string format
function utils.hostname()
  if hostname ~= nil then
    return hostname
  end

  local file = io.open("/etc/hostname", "r")
  if file == nil then
    return ""
  end

  local res = file:read()
  if res == nil then
    return ""
  end

  file:close()

  hostname = res
  return res
end

return utils
