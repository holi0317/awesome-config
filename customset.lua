-- Host-sepcific startup functions
local awful = require("awful")
local utils = require("utils")

local host = utils.hostname()
if host == "holi-XPS-13-9350.arch" then
  awful.spawn.once("xset s 520 60")
end
