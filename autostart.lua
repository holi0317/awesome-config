local awful = require("awful")
local globals = require("globals")
local utils = require("utils")

-- Do not start anything if under vnc environment. Otherwise performance would be horrible
if os.getenv("TIGER_VNC") == "1" then
  return
end

-- Try to spawn a program. If that does not exist, do nothing.
-- @param bin Name of the binary of the program
-- @param flags All flags (separated with space) that should be used for executing bin
local function try_spawn(bin, flags)
  awful.spawn.easy_async(string.format("which %s", bin), function(res)
    if res == "" then
      return
    end

    local cmd = bin
    if flags ~= nil then
      cmd = bin .. " " .. bin
    end

    awful.spawn.once(cmd)
  end)
end

-- Lock screen starter
local xss_lock = string.format("xss-lock --ignore-sleep -- %s", globals.locker)
if utils.hostname() == "holi-XPS-13-9350.arch" then
  local dim_script = os.getenv("HOME") .. "/.config/awesome/dim-screen.sh"
  xss_lock = string.format("xss-lock -n %s -- %s", dim_script, globals.locker)
end
awful.spawn.once(xss_lock)

-- NetworkManager applet (Notification tray)
try_spawn("nm-applet")

-- Windows composition
awful.spawn.once("compton -b")

-- Volume manager and file manager
awful.spawn.once("pcmanfm-qt -d --desktop")

-- Nextcloud folder syncing
try_spawn("nextcloud")

-- KDE connect daemon
try_spawn("kdeconnect-indicator")

-- Telegram desktop
try_spawn("telegram-desktop", "-startintray")

-- Flash focused window
try_spawn("flashfocus")
