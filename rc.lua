local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
local beautiful = require("beautiful")
local naughty = require("naughty")
local colors = require("colors")

local keys = require("keys")
require("autostart")
require("customset")

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
  naughty.notify({
    preset = naughty.config.presets.critical,
    title = "Oops, there were errors during startup!",
    text = awesome.startup_errors
  })
end

-- Handle runtime errors after startup
do
  local in_error = false
  awesome.connect_signal("debug::error", function(err)
    -- Make sure we don't go into an endless error loop
    if in_error then
      return
    end
    in_error = true

    naughty.notify({
      preset = naughty.config.presets.critical,
      title = "Oops, an error happened!",
      text = tostring(err)
    })
    in_error = false
  end)
end
-- }}}

-- {{{ Variable definitions
-- Themes define colours, icons, font and wallpapers.
beautiful.init(os.getenv("HOME") .. "/.config/awesome/theme.lua")

-- Table of layouts to cover with awful.layout.inc, order matters.
awful.layout.layouts = {
  awful.layout.suit.floating,
  awful.layout.suit.tile,
  --awful.layout.suit.tile.left,
  --awful.layout.suit.tile.bottom,
  --awful.layout.suit.tile.top,
  --awful.layout.suit.fair,
  --awful.layout.suit.fair.horizontal,
  --awful.layout.suit.spiral,
  --awful.layout.suit.spiral.dwindle,
  awful.layout.suit.max,
  --awful.layout.suit.max.fullscreen,
  --awful.layout.suit.magnifier,
  awful.layout.suit.corner.nw
  -- awful.layout.suit.corner.ne,
  -- awful.layout.suit.corner.sw,
  -- awful.layout.suit.corner.se,
}
-- }}}

-- {{{ Wibar
-- Create a wibox for each screen and add it
local function set_wallpaper(s)
  -- Wallpaper
  if beautiful.wallpaper then
    local wallpaper = beautiful.wallpaper
    -- If wallpaper is a function, call it with the screen
    if type(wallpaper) == "function" then
      wallpaper(function(path)
        if path ~= nil then
          gears.wallpaper.maximized(path, s, true)
        end
      end)
      return
    end
    gears.wallpaper.maximized(wallpaper, s, true)
  end
end

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", set_wallpaper)

awful.screen.connect_for_each_screen(function(s)
  -- Wallpaper
  set_wallpaper(s)

  -- Create tag table for each screen
  local names = {
    "",
    "",
    "",
    "",
    "",
  }
  local layouts = {
    awful.layout.suit.max,
    awful.layout.magnifier,
    awful.layout.magnifier,
    awful.layout.tile,
    awful.layout.suit.max
  }
  awful.tag(names, s, layouts)

  beautiful.at_screen_connect(s)
end)
-- }}}

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
  -- All clients will match this rule.
  {
    rule = {},
    except = {
      type = "desktop"
    },
    properties = {
      border_width = beautiful.border_width,
      border_color = beautiful.border_normal,
      focus = awful.client.focus.filter,
      raise = true,
      keys = keys.clientkeys,
      buttons = keys.clientbuttons,
      screen = awful.screen.preferred,
      placement = awful.placement.no_overlap + awful.placement.no_offscreen,
      maximized_vertical   = false,
      maximized_horizontal = false
    }
  },
  -- Floating clients.
  {
    rule_any = {
      -- First string of xprop WM_CLASS
      instance = {
        "DTA", -- Firefox addon DownThemAll.
        "copyq", -- Includes session name in class.
        "nm-connection-editor",
        "yubioath-desktop"
      },
      -- Second string of xprop WM_CLASS
      class = {
        "Arandr",
        "Gpick",
        "Kruler",
        "MessageWin", -- kalarm.
        "Sxiv",
        "Wpa_gui",
        "pinentry",
        "veromix",
        "xtightvncviewer",
        "mpv"
      },
      name = {
        "Event Tester" -- xev.
      },
      role = {
        "AlarmWindow", -- Thunderbird's calendar.
        "pop-up" -- e.g. Google Chrome's (detached) Developer Tools.
      }
    },
    properties = {floating = true}
  },
  -- Floating Thunderbird dialogs
  {
    rule = { class = "Thunderbird" },
    except = { instance = "Mail" },
    properties = {floating = true}
  },
  -- Center dialogs
  {
    rule_any = {
      instance = {
        "pinentry-gtk-2",
        "pinentry-gnome3",
        "pinentry-qt"
      }
    },
    properties = { placement = awful.placement.centered }
  },
  -- pcmanfm-qt desktop root window
  {
    rule = {
      type = "desktop"
    },
    properties = {
      border_width = 0,
      border_color = colors.black,
      sticky = true,
      keys = nil,
      buttons = nil
    }
  },
  -- Telegram dialog
  {
    rule = {
      type = "dialog",
      instance = "telegram-desktop"
    },
    properties = {
      border_color = colors.black,
      border_width = 0
    }
  }
}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function(c)
  -- Set the windows at the slave,
  -- i.e. put it at the end of others instead of setting it master.
  -- if not awesome.startup then awful.client.setslave(c) end

  if awesome.startup and not c.size_hints.user_position and not c.size_hints.program_position then
    -- Prevent clients from being unreachable after screen count changes.
    awful.placement.no_offscreen(c)
  end
end)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
  if awful.layout.get(c.screen) ~= awful.layout.suit.magnifier and awful.client.focus.filter(c) then
    client.focus = c
  end
end)

client.connect_signal("focus", function(c)
  c.border_color = beautiful.border_focus
end)
client.connect_signal("unfocus", function(c)
  c.border_color = beautiful.border_normal
end)

-- Center client when it is changed to floating mode
client.connect_signal("property::floating", function(c)
  if not c.fullscreen then
    awful.placement.centered(c, {honor_workarea = true})
  end
end)

if beautiful.border_radius > 0 then
  client.connect_signal("manage", function (c)
    if not c.fullscreen then
      c.shape = beautiful.border_shape
    end
  end)

  -- Fullscreen clients should not have rounded corners
  client.connect_signal("property::fullscreen", function (c)
    if c.fullscreen then
      c.shape = gears.shape.rectangle
    else
      c.shape = beautiful.border_shape
    end
  end)
end

-- }}}
