local globals = {}

globals.modkey = "Mod4"

-- Application settings
globals.terminal = "alacritty"
globals.editor = os.getenv("EDITOR") or "nano"
globals.editor_cmd = globals.terminal .. " -e " .. globals.editor
globals.locker = "betterlockscreen -l -- --nofork"
globals.screenshot = "flameshot gui -p ~/Pictures/"

globals.fontname = "Hack Nerd Font Mono "
globals.font = globals.fontname .. "12"

return globals
